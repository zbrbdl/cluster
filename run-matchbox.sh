#!/usr/bin/env sh
set -e

SUDO="sudo UNIFIED_CGROUP_HIERARCHY=0"

${SUDO} rkt run \
    --net=host \
    --mount="volume=data,target=/var/lib/matchbox" \
    --volume="data,kind=host,source=${PWD}/matchbox" \
    quay.io/coreos/matchbox:latest \
    --mount="volume=config,target=/etc/matchbox" \
    --volume="config,kind=host,source=${PWD}/etc,readOnly=true" \
    -- \
        -address=0.0.0.0:8080 \
        -rpc-address=0.0.0.0:8081 \
        -log-level=debug

